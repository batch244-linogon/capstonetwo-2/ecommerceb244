const expressJwt = require('express-jwt');

function authJwt() {
    const secret = process.env.secret;
    const api = process.env.API_URL;
    return expressJwt({
        secret: "EcommerceAPI",
        algorithms: ['HS256'],
        isRevoked: isRevoked
    }).unless({
        path: [
            //{url: /\/public\/uploads(.*)/ , methods: ['GET', 'OPTIONS'] },

           
            {url: /\/api\/v1\/products(.*)/ , methods: ['POST', 'OPTIONS', 'DELETE', 'GET', 'PATCH'] },
           

            {url: /\/api\/v1\/categories(.*)/ , methods: ['POST', 'OPTIONS', 'DELETE', 'GET', 'PATCH'] },
            {url: /\/api\/v1\/orders(.*)/,methods: ['POST', 'OPTIONS', 'DELETE', 'GET', 'PATCH']},
            `${api}/users/login`,
            `${api}/users/register`, 
        ]
    })
}

async function isRevoked(req, payload, done) {
    if(!payload.isAdmin) {
        done(null, true)
    }

    done();
}



module.exports = authJwt