const mongoose = require('mongoose');
/*
User
 -Email (String)
 -Password(String)
 -isAdmin(Boolean-defaults to false)
 -orders(Array of objects)
    1.products(Array of objects)
       -productName(Array of objects)
       -quantity(Number)
    2.totalAmount(Number)
    3.purchasedOn(Date-defaults to current timestamp)
*/
//==================================================
/*
const userSchema = new mongoose.Schema({
    Name: {
        type: String,
        required: true,
    },
    Email: {
        type: String,
        required: true,
    },
    PasswordHash: {
        type: String,
        required: false,
    },
    isAdmin: {
        type: Boolean,
        default: false,
    },
    
   orders: [
          {
            products: [
                {
                    productName:{
                        type:String,
                        required:[true, "Product name is required"]
                    },
                    quantity:{
                        type:Number,
                        required:[true, "Quantity is required"]
                    }
                }
            ],
            totalAmount:{
                type:Number,
                required:[true, "Total amount is required"]
                },
            purchasedOn:{
                type:Date,
                default: new Date()
                }
            }
        
    ]

    
});
  
userSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

userSchema.set('toJSON', {
    virtuals: true,
});

exports.User = mongoose.model('User', userSchema);
exports.userSchema = userSchema;
*/



const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    passwordHash: {
        type: String,
        required: true,
    },
    phone: {
        type: String,
        required: true,
    },
    isAdmin: {
        type: Boolean,
        default: false,
    },
    street: {
        type: String,
        default: ''
    },
    apartment: {
        type: String,
        default: ''
    },
    zip :{
        type: String,
        default: ''
    },
    city: {
        type: String,
        default: ''
    },
    country: {
        type: String,
        default: ''
    }

});

userSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

userSchema.set('toJSON', {
    virtuals: true,
});

exports.User = mongoose.model('User', userSchema);
exports.userSchema = userSchema;

