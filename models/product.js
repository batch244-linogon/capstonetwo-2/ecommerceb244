const mongoose = require('mongoose');

/*
**Data model**


Product
  -Name(String)
  -Description(String)
  -Price(Number)
  -isActive(Booleans-defaults to current timestamp)
  -order(Array of Objects)
     orderId(String)
*/

//Start of Schema================================
/*
const productSchema = mongoose.Schema({
    Name: {
        type: String,
        required: true,
    },
    Description: {
        type: String,
        required: true
    },
    
    Price : {
        type: Number,
        default:0
    },
    isActive: {
        type: Boolean,
        default: true,
        //default: Date.now,
    },
    createdOn:{
                type:Date,
                default: new Date()
    },

    orders: [
        {
                products1: [
                 {
                    orderId:{
                        type:String,
                        required:[true, "order ID is required"]
                    },
                    quantity:{
                        type:Number,
                        required:[true, "Quantity is required"]
                        }
                    }
                ],
                products2: [
                 {
                    orderId:{
                        type:String,
                        required:[true, "order ID is required"]
                    },
                    quantity:{
                        type:Number,
                        required:[true, "Quantity is required"]
                        }
                    }
                ],
                products3: [
                 {
                    orderId:{
                        type:String,
                        required:[true, "order ID is required"]
                    },
                    quantity:{
                        type:Number,
                        required:[true, "Quantity is required"]
                        }
                    }
                ],
            
            
        }
        
    ]
})//End of Schema=========================================
*/
const productSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true
    },
    richDescription: {
        type: String,
        default: ''
    },
    image: {
        type: String,
        default: ''
    },
    images: [{
        type: String
    }],
    brand: {
        type: String,
        default: ''
    },
    price : {
        type: Number,
        default:0
    },
    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category',
        required:true
    },
    countInStock: {
        type: Number,
        required: true,
        min: 0,
        max: 255
    },
    rating: {
        type: Number,
        default: 0,
    },
    numReviews: {
        type: Number,
        default: 0,
    },
    isFeatured: {
        type: Boolean,
        default: false,
    },
    dateCreated: {
        type: Date,
        default: Date.now,
    },
})  
//==============================================
productSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

productSchema.set('toJSON', {
    virtuals: true,
});


exports.Product = mongoose.model('Product', productSchema);
